import { Component } from '@angular/core';
import {AdminStyle1} from "../../admin-style";

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [],
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss', ...AdminStyle1]
})
export class HomeComponent {

}
