import {
  Component,
  OnInit
} from '@angular/core';
import {SharedModule} from "../shared-module";
import {HttpClient} from "@angular/common/http";
import {NgOptimizedImage} from "@angular/common";
import {
  AdminLibBaseCss1,
} from "../../admin-style";
import {NgxSpinnerService} from "ngx-spinner";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-profile',
  standalone: true,
  imports: [SharedModule, NgOptimizedImage],
  providers: [ToastrService],
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss', ...AdminLibBaseCss1]
})
export class ProfileComponent implements OnInit {
  currentUser: any = {
    username: '',
    email: '',
    phone: '',
    address: '',
    avatar: '',
    s3Profile: '',
    dob: ''
  };
  avatarSrc: string = 'assets/images/faces/face6.jpg';
  formData = new FormData();
  showDropdown = false;
  isTabInfo: boolean = true;

  paramPassword: any = {
    oldPassword: '',
    newPassword: '',
  }

  constructor(private http: HttpClient,
              private spin: NgxSpinnerService,
              private toast: ToastrService) {
  }

  ngOnInit(): void {
    this.getProfile();
  }

  getProfile() {
    this.http.get('/api/users')
      .subscribe((res: any) => {
        this.currentUser = res;
        // mai mot thay bang  res?.s3Profile
        // this.avatarSrc = this.currentUser?.avatar;
      });
  }

  updateInfo() {
    this.spin.show();
    this.http.patch('/api/users/update-profile-user', this.currentUser)
      .subscribe((res: any) => {
        this.spin.hide();
        if(res?.statusCode == 200) {
          this.toast.success('Cập nhật thông tin thành công');
        }
        this.getProfile();
      });
  }
}
