import {
  Component,
  OnInit
} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {SharedModule} from "../shared-module";
import {
  AdminLibBaseCss1,
  AdminStyle1
} from "../../admin-style";
import {NgOptimizedImage} from "@angular/common";

@Component({
  selector: 'app-sidebar',
  standalone: true,
  imports: [
    SharedModule,
    NgOptimizedImage
  ],
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss', ...AdminStyle1, ...AdminLibBaseCss1]
})
export class SidebarComponent implements OnInit {
  navItemActive: string = 'dashboard';
  stateMenu: boolean[] = Array(10).fill(false);

  constructor(private http: HttpClient) {
  }

  ngOnInit(): void {
    this.getNavItems();

  }

  private getNavItems(): void {
    const url = window.location.href;
    if (url.includes('dashboard') || url.includes('home')) {
      this.navItemActive = 'dashboard';
    } else if (url.includes('users')) {
      this.navItemActive = 'users';
    } else if (url.includes('topic')) {
      this.navItemActive = 'topic';
    } else if (url.includes('exam') || url.includes('question')) {
      this.navItemActive = 'exam';
    } else if (url.includes('score')) {
      this.navItemActive = 'score';
    } else if (url.includes('logout')) {
      this.navItemActive = 'logout';
    } else if (url.includes('firebase') || url.includes('update-firebase')) {
      this.navItemActive = 'firebase';
    }
  }
}
