import {
  Component,
  Input,
  OnInit
} from '@angular/core';
import {SharedModule} from "../../shared-module";
import {
  AdminLibBaseCss2,
  AdminStyle2
} from "../../../admin-style";
import {HttpClient} from "@angular/common/http";
import {BsModalRef} from "ngx-bootstrap/modal";

@Component({
  selector: 'app-update-user',
  standalone: true,
  imports: [SharedModule],
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.scss', ...AdminStyle2, ...AdminLibBaseCss2]
})
export class UpdateUserComponent implements OnInit {
  @Input() user: any = {};

  constructor(private http: HttpClient,
              private bsModalRef: BsModalRef) {
  }

  ngOnInit(): void {
  }

  close() {
    this.bsModalRef.hide();
  }

  doUpdateUser() {
    console.log(this.user);
  }
}
