import {
  Component,
  OnInit,
} from '@angular/core';
import {PageHeaderComponent} from "../page-header/page-header.component";
import {
  AdminLibBaseCss1,
  AdminStyle1
} from "../../admin-style";
import {SharedModule} from "../shared-module";
import {HttpClient} from "@angular/common/http";
import {NgOptimizedImage} from "@angular/common";
import {PopoverModule} from "ngx-bootstrap/popover";
import {BsModalService} from "ngx-bootstrap/modal";
import {UpdateUserComponent} from "./update-user/update-user.component";

@Component({
  selector: 'app-user',
  standalone: true,
  imports: [
    PageHeaderComponent,
    SharedModule,
    NgOptimizedImage,
    PopoverModule
  ],
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss', ...AdminStyle1, ...AdminLibBaseCss1]
})
export class UserComponent implements OnInit {
  listUser: any = [];
  title = 'Quản lý tài khoản';
  currentPage = 'Tài khoản';
  page = 1;
  take = 10;
  textSearch = '';


  constructor(private http: HttpClient,
              private bsModal: BsModalService) {
  }

  ngOnInit(): void {
    this.getListUser();
  }

  getListUser(freeText: string = ''): void {
    this.http.get(`/api/cms-users/get-users?page=${this.page}&take=${this.take}&freeText=${freeText}`)
      .subscribe((res: any) => {
        this.listUser = res?.data;
      });
  }


  search(event: any): void {
    this.getListUser(event);
  }

  openFormEdit(item: any) {
    this.bsModal.show(UpdateUserComponent, {
      class: 'modal-lg modal-dialog-centered',
      initialState: {
        user: item
      }
    });
  }

  doDelete(id: any) {
    const confirm = window.confirm('Bạn có chắc chắn muốn xóa tài khoản này không?');
    if (!confirm) {
      return;
    }

    this.http.delete(`/api/cms-users/${id}`)
      .subscribe((res: any) => {
        this.getListUser();
      });
  }
}
