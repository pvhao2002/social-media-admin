import {Routes} from '@angular/router';
import {AppComponent} from "./app.component";
import {HomeComponent} from "./home/home.component";
import {UserComponent} from "./user/user.component";
import {UpdateUserComponent} from "./user/update-user/update-user.component";
import {ProfileComponent} from "./profile/profile.component";

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: HomeComponent,
  },
  {
    path: 'user',
    children: [
      {
        path: 'list',
        component: UserComponent
      }, {
        path: 'update',
        component: UpdateUserComponent
      }
    ]
  },
  {
    path: 'profile',
    component: ProfileComponent
  }
];
