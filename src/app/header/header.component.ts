import {Component} from '@angular/core';
import {
  NgClass,
  NgOptimizedImage
} from "@angular/common";
import {
  AdminLibBaseCss1,
  AdminStyle1
} from "../../admin-style";
import {SharedModule} from "../shared-module";

@Component({
  selector: 'app-header',
  standalone: true,
  imports: [
    NgOptimizedImage,
    SharedModule
  ],
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss', ...AdminStyle1, ...AdminLibBaseCss1]
})
export class HeaderComponent {
  isShowMenu: boolean = false;
}
