import {Component} from '@angular/core';
import {RouterOutlet} from '@angular/router';
import {SharedModule} from "./shared-module";
import {HeaderComponent} from "./header/header.component";
import {SidebarComponent} from "./sidebar/sidebar.component";
import {FooterComponent} from "./footer/footer.component";
import {
  AdminLibBaseCss,
  AdminStyle
} from "../admin-style";


@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, SharedModule, HeaderComponent, SidebarComponent, FooterComponent],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css', ...AdminStyle, ...AdminLibBaseCss]
})
export class AppComponent {
  title = 'Social-media-admin';
}
