import { Component } from '@angular/core';
import {
  AdminLibBaseCss1,
  AdminStyle1
} from "../../admin-style";

@Component({
  selector: 'app-footer',
  standalone: true,
  imports: [],
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss', ...AdminStyle1, ...AdminLibBaseCss1]
})
export class FooterComponent {
  date: Date = new Date();
}
