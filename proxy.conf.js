const PROXY_CONFIG = [
  {
    context: [
      "/api",
    ],
    target: "http://192.168.1.32:9200",
    secure: false,
    "changeOrigin": true,
    "logLevel": "debug",
    headers: {host: 'localhost'},
    pathRewrite: {"^/api": "/api"}
  }
];
module.exports = PROXY_CONFIG;
